import subprocess
import os

def mountHardDrives():
    hardDrives = [
        # You can get this info by running sudo blkid on terminal
        ( "Backup partition", 'UUID="01D53130CDC88740"', "/mnt/Steamie", "ntfs", "defaults", "0 0" ),
        ( "Windows install", 'UUID="00924AF3924AED2A"', "/mnt/wininstall", "ntfs", "defaults", "0 0" ),
        ( "Swap partition", 'UUID="93f46149-8eb8-4a80-9b6d-0b14c918ebde"', "none", "swap", "sw","0 0")
    ]
    
    # creates folders where hard drive files will be mounted at
    createMntPpints = subprocess.run( "zsh createMntPoints.sh", capture_output=True, shell=True )
    if createMntPpints.returncode == 0:
        os.system(
            f'echo "\n$(tput setaf 46)$(tput bold)Mount points created !$(tput sgr0)"')

    for diskName, diskUUID, mountPoint, storageFormat, defaults, zeros in hardDrives:
        with open( "/etc/fstab", 'a' ) as mntDisk:
            mntDisk.write( f"\n#{diskName}" )
            mntDisk.write( f'\n{diskUUID} {mountPoint} {storageFormat} {defaults} {zeros}\n' )

    os.system(
        f'echo "\n$(tput setaf 46)$(tput bold)Hard drive info has been written to your fstab!$(tput sgr0)"')
