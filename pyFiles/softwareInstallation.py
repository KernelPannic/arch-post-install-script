import os
import subprocess
import json

'''
Loads package name for printing and package binary name the one you use for installing packages on arch
Arch package names can be found here: https://www.archlinux.org/packages/
Ubuntu package names can be found here: https://packages.ubuntu.com/
You can check packages.json to add/remove packages
'''
def packageInstallation():

    def installPackage( packageBinary ):
        # Messages
        successfulyInstalledPackage = f"{packageBinary} was sucessfully installed !"
        failedPackageInstallation = f"Something went wrong when installing {packageBinary} :("

        # Install commands
        runPacmanS = f"pacman -S {packageBinary} --noconfirm" #runs Pacman -S package command
        install = subprocess.run( runPacmanS, capture_output=True, shell=True );

        if install.returncode == 0:
            os.system( f'echo "$(tput bold)$(tput setaf 46) {successfulyInstalledPackage}$(tput sgr0)\n"' )
        else:
            os.system( f'echo "$(tput bold)$(tput setaf 196){failedPackageInstallation}$(tput sgr0)\n"' )

    with open( "jsonFiles/packages.json", "r" ) as jsonFile:
        packages = json.load( jsonFile )
    
    sucessInstall=1 #Keeps tracking on how many packages have been installed
    for packageName, packageBinaryName in packages.items():

        #runs which command to check out if the current package binary already exists on your system
        checkInstallation = subprocess.run( f"which {packageBinaryName}",capture_output=True, text=True, shell=True )
        alreadyInstalledPackage = f'$(tput bold)$(tput setaf 11){packageBinaryName} is already installed :) $(tput sgr0)\n'
        installingMessage = f'echo "$(tput bold)$(tput setaf 38) Installing {packageName} [{sucessInstall}/{len(packages)}]$(tput sgr0)"'

        if( checkInstallation.returncode == 1 ):
            sucessInstall+=1
            os.system( installingMessage )
            installPackage( packageBinaryName )
        else:
            os.system( installingMessage )
            os.system( f'echo "{alreadyInstalledPackage}"' )