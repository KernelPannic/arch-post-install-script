import os
import subprocess
import json

def exportAliasesToRcFile():
    user = "glaze" # set your user here
    with open( "jsonFiles/aliases.json", "r" ) as jsonFile:
        aliases = json.load( jsonFile )

    checkZshrcFileExistence = subprocess.run( f"ls /home/{user}/.zshrc", capture_output=True, shell=True )

    if checkZshrcFileExistence.returncode == 0:
        with open( f"/home/{user}/.zshrc", "a" ) as file:
            file.write("# Aliases")
            for alias, command in aliases.items():
                file.write( f'\nalias {alias}="{command}"' )
        os.system( f'echo "\n$(tput bold)$(tput setaf 46)Aliases added!$(tput sgr0)"' )
    else:
        os.system( f'echo "$(tput bold)($tput setaf 196){checkZshrcFileExistence.stderr}$(tput sgr0)"' )