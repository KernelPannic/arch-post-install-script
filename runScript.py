import subprocess

from pyFiles.aliases import exportAliasesToRcFile
from pyFiles.mountDisks import mountHardDrives
from pyFiles.softwareInstallation import packageInstallation

'''
-> THIS SCRIPT MUST BE RUN AS ROOT !!!

This is script is designed to run under an Arch Linux based operatying systems, if you want to run this script 
on a debian based OS like Ubuntu, Linux Mint among others debian derivates, you must tweak some settings
in order to make it work:

-> edit command variable on softwareInstallation file to:
   runPacmanS = f"apt-get {packageBinaryName} -y"
-> replace package binaries on jsonFiles/packages.json by the ones on your system, by example
   firefox-developer-edition doesn't exist on ubuntu repos
'''

#Synchronizes package databases and fully updates your system, perfect for a fresh install
#for debian based systems: echo {sudoPassword}|sudo -S apt-update
subprocess.run( f'pacman -Syu --noconfirm', shell=True )

# check softwareInstallation for info on what this function does
packageInstallation()

#Add all of your aliases to your respective rc file
exportAliasesToRcFile()

#Adds the requeried info on your fstab file to keep your disk mounted at start up
mountHardDrives()
